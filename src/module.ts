import fetch from 'isomorphic-fetch';

enum FetchMethod {
    POST = "POST",
    GET = "GET"
}

class AuthHandler {

    private authhost: string = 'https://auth.scriptless.io';
    private secret: string = '';
    private jwt: string = '';

    constructor(authhost: string, secret: string, jwt: string = '') {
        this.setHost(authhost);
        this.secret = secret;
        this.jwt = jwt;
    }

    /*
    *
    *   NICHT VERGESSEN: YARN BUILD vor dem PUBLISHEN
    *
    */
    
    setHost = (authhost: string) => {
        if(!authhost.startsWith('http')) return;
        this.authhost = authhost;
    }
    
    getHost = () => {
        return this.authhost;
    }
    
    setSecret = (secret: string) => {
        this.secret = secret;
    }

    getSecret = () => {
        return this.secret;
    }

    setJWT = (jwt: string) => {
        this.jwt = jwt;
    }

    getJWT = () => {
        return this.jwt;
    }

    isLoggedIn = async (): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.GET, '/status');

        // error checking
        if(!response) return false;
        if(!response.success) return false;
        return true;
    }

    logout = async (): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/logout');
        
        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    login = async (user: string, password: string, remember: boolean = false): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/login', {
            user,
            password,
            remember
        });

        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    register = async (email: string, username: string, password: string, captcha: string): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/register', {
            email,
            username,
            password,
            captcha
        });

        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    registerConfirm = async (token: string): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/register/confirm/ ' + token);

        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    requestReset = async (email: string, captcha: string): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/resetpw', {
            email,
            captcha
        });

        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    confirmReset = async (token: string, password: string): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/resetpw/confirm', {
            token,
            password
        });

        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    changePassword = async (password: string, captcha: string): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/changepw', {
            password,
            captcha
        });

        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    deleteAccount = async (): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/delete');

        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    deleteAccountElse = async (id: string): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.POST, '/delete/' + id);

        // error checking
        if(!response) return false;
        if(!response.success) return new Error(response.message);

        return true;
    }

    getData = async (field: string): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.GET, '/status');

        // error checking
        if(!response) return null;
        if(!response.success) return null;
        
        return response.data[field];
    }

    getUser = async (): Promise<any> => {
        const response = await this.makeFetch(FetchMethod.GET, '/user');

        // error checking
        if(!response) return null;
        if(!response.success) return null;
        
        return response.data.user;
    }

    private makeFetch = async (method: FetchMethod, path: string, body: object = {}) => {

        const redirect = 'follow';
        const credentials = 'include';
        
        let headers: any = {
            'Auth-Secret': this.secret,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        if(this.jwt !== '') {
            headers['Authorization'] = 'Bearer ' + this.jwt;
        }

        if(method === FetchMethod.GET) {
            return await fetch(this.authhost.concat(path), {
                method,
                redirect,
                credentials,
                headers
            })
            .then(response => response.json())
            .catch(err => {
                console.log("[AuthHandler] GET Fetch error (" + path + "): " + err);
                return false;
            });
        }

        return await fetch(this.authhost.concat(path), {
            method,
            redirect,
            credentials,
            headers,
            body: JSON.stringify(body)
        })
        .then(response => response.json())
        .catch(err => {
            console.log("[AuthHandler] POST Fetch error (" + path + "): " + err);
            return false;
        });
    }
}

export { AuthHandler };