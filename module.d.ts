declare class AuthHandler {
    private authhost;
    private secret;
    private jwt;
    constructor(authhost: string, secret: string, jwt?: string);
    setHost: (authhost: string) => void;
    getHost: () => string;
    setSecret: (secret: string) => void;
    getSecret: () => string;
    setJWT: (jwt: string) => void;
    getJWT: () => string;
    isLoggedIn: () => Promise<any>;
    logout: () => Promise<any>;
    login: (user: string, password: string, remember?: boolean) => Promise<any>;
    register: (email: string, username: string, password: string, captcha: string) => Promise<any>;
    registerConfirm: (token: string) => Promise<any>;
    requestReset: (email: string, captcha: string) => Promise<any>;
    confirmReset: (token: string, password: string) => Promise<any>;
    changePassword: (password: string, captcha: string) => Promise<any>;
    deleteAccount: () => Promise<any>;
    deleteAccountElse: (id: string) => Promise<any>;
    getData: (field: string) => Promise<any>;
    getUser: () => Promise<any>;
    private makeFetch;
}
export { AuthHandler };
