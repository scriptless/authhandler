"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var isomorphic_fetch_1 = __importDefault(require("isomorphic-fetch"));
var FetchMethod;
(function (FetchMethod) {
    FetchMethod["POST"] = "POST";
    FetchMethod["GET"] = "GET";
})(FetchMethod || (FetchMethod = {}));
var AuthHandler = /** @class */ (function () {
    function AuthHandler(authhost, secret, jwt) {
        var _this = this;
        if (jwt === void 0) { jwt = ''; }
        this.authhost = 'https://auth.scriptless.io';
        this.secret = '';
        this.jwt = '';
        /*
        *
        *   NICHT VERGESSEN: YARN BUILD vor dem PUBLISHEN
        *
        */
        this.setHost = function (authhost) {
            if (!authhost.startsWith('http'))
                return;
            _this.authhost = authhost;
        };
        this.getHost = function () {
            return _this.authhost;
        };
        this.setSecret = function (secret) {
            _this.secret = secret;
        };
        this.getSecret = function () {
            return _this.secret;
        };
        this.setJWT = function (jwt) {
            _this.jwt = jwt;
        };
        this.getJWT = function () {
            return _this.jwt;
        };
        this.isLoggedIn = function () { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.GET, '/status')];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, false];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.logout = function () { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/logout')];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, new Error(response.message)];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.login = function (user, password, remember) {
            if (remember === void 0) { remember = false; }
            return __awaiter(_this, void 0, void 0, function () {
                var response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/login', {
                                user: user,
                                password: password,
                                remember: remember
                            })];
                        case 1:
                            response = _a.sent();
                            // error checking
                            if (!response)
                                return [2 /*return*/, false];
                            if (!response.success)
                                return [2 /*return*/, new Error(response.message)];
                            return [2 /*return*/, true];
                    }
                });
            });
        };
        this.register = function (email, username, password, captcha) { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/register', {
                            email: email,
                            username: username,
                            password: password,
                            captcha: captcha
                        })];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, new Error(response.message)];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.registerConfirm = function (token) { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/register/confirm/ ' + token)];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, new Error(response.message)];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.requestReset = function (email, captcha) { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/resetpw', {
                            email: email,
                            captcha: captcha
                        })];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, new Error(response.message)];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.confirmReset = function (token, password) { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/resetpw/confirm', {
                            token: token,
                            password: password
                        })];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, new Error(response.message)];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.changePassword = function (password, captcha) { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/changepw', {
                            password: password,
                            captcha: captcha
                        })];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, new Error(response.message)];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.deleteAccount = function () { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/delete')];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, new Error(response.message)];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.deleteAccountElse = function (id) { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.POST, '/delete/' + id)];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, false];
                        if (!response.success)
                            return [2 /*return*/, new Error(response.message)];
                        return [2 /*return*/, true];
                }
            });
        }); };
        this.getData = function (field) { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.GET, '/status')];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, null];
                        if (!response.success)
                            return [2 /*return*/, null];
                        return [2 /*return*/, response.data[field]];
                }
            });
        }); };
        this.getUser = function () { return __awaiter(_this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.makeFetch(FetchMethod.GET, '/user')];
                    case 1:
                        response = _a.sent();
                        // error checking
                        if (!response)
                            return [2 /*return*/, null];
                        if (!response.success)
                            return [2 /*return*/, null];
                        return [2 /*return*/, response.data.user];
                }
            });
        }); };
        this.makeFetch = function (method, path, body) {
            if (body === void 0) { body = {}; }
            return __awaiter(_this, void 0, void 0, function () {
                var redirect, credentials, headers;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            redirect = 'follow';
                            credentials = 'include';
                            headers = {
                                'Auth-Secret': this.secret,
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            };
                            if (this.jwt !== '') {
                                headers['Authorization'] = 'Bearer ' + this.jwt;
                            }
                            if (!(method === FetchMethod.GET)) return [3 /*break*/, 2];
                            return [4 /*yield*/, isomorphic_fetch_1.default(this.authhost.concat(path), {
                                    method: method,
                                    redirect: redirect,
                                    credentials: credentials,
                                    headers: headers
                                })
                                    .then(function (response) { return response.json(); })
                                    .catch(function (err) {
                                    console.log("[AuthHandler] GET Fetch error (" + path + "): " + err);
                                    return false;
                                })];
                        case 1: return [2 /*return*/, _a.sent()];
                        case 2: return [4 /*yield*/, isomorphic_fetch_1.default(this.authhost.concat(path), {
                                method: method,
                                redirect: redirect,
                                credentials: credentials,
                                headers: headers,
                                body: JSON.stringify(body)
                            })
                                .then(function (response) { return response.json(); })
                                .catch(function (err) {
                                console.log("[AuthHandler] POST Fetch error (" + path + "): " + err);
                                return false;
                            })];
                        case 3: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        this.setHost(authhost);
        this.secret = secret;
        this.jwt = jwt;
    }
    return AuthHandler;
}());
exports.AuthHandler = AuthHandler;
